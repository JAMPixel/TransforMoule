//
// Created by kelmyn on 18-03-03.
//

#include <cmath>
#include <iostream>
#include <inc/2dEngine/keyEventHandler.hpp>
#include <inc/2dEngine/scene.hpp>
#include <inc/2dEngine/gameState.hpp>
#include <fish/fish.hpp>
#include <inc/2dEngine/eventManager.hpp>
#include <inc/2dEngine/sound.hpp>
#include <glm/glm.hpp>
#include <inc/2dEngine/programManager.hpp>
#include "Mussel.h"
#include "world/world.h"
#include "FishEggs.h"

Mussel::Mussel(): Entity(8.0, 4.5, 0.35, 0.35){
    setSpriteSheet("mBase", 0.5);
    weight = baseWeight;
    impulseStrength = baseStrength;
    angle = 0.0f;
    impulseAllowed = true;
    impulseOrder = false;
    layer = "mussel";

    JamEngine::KeyEventHandler::addKeyEventPressed("jump", [this](){
	    impulse();
    });
    JamEngine::KeyEventHandler::addKeyEventPressed("turnRight", [this](){ beginRotateRight(); });
    JamEngine::KeyEventHandler::addKeyEventReleased("turnRight", [this](){ endRotateRight(); });
    JamEngine::KeyEventHandler::addKeyEventPressed("turnLeft", [this]{ beginRotateLeft(); });
    JamEngine::KeyEventHandler::addKeyEventReleased("turnLeft", [this](){ endRotateLeft(); });

	JamEngine::EventManager::addEvent(-1, [this](int i){
		glm::vec2 distance = glm::abs(pos - JamEngine::ProgramManager::getPosCam());

		float delta = 0.15;
		if(distance.y < 2 ){
			ProgramManager::moveCam(0, -delta);
		}else if(distance.y > 7){
			ProgramManager::moveCam(0, delta);
		}

		if(distance.x < 2 ){
			ProgramManager::moveCam(-0.15, 0);
		}else if(distance.x > 14){
			ProgramManager::moveCam(0.15, 0);
		}


	});
}

Mussel::~Mussel(){

}

void Mussel::collide(Entity &other) {
    bool collides = false;

    if (abs(other.getPosX() - getPosX()) < other.getSizeX() + getSizeX()){
        if (abs(other.getPosY() - getPosY()) < other.getSizeY() + getSizeY()){
            collides = true;
        }
    }

    if (collides){
        // TODO: Do something
        if(dynamic_cast<Fish *>(&other)){
            std::cout << "you loose " << std::endl;
            isDead = true;
        }else if(dynamic_cast<FishEggs *>(&other)){
            EventManager::addEvent(0, [](int i){
               Sound::play("pick", 0.8);
            });
            player.addFishEgg();
            scene.destroy(&other);
            std::cout << "you have collected one Egg " << std::endl;
        }

    }
}

// Entity
void Mussel::update(){
    updatePosition();

}

void Mussel::updatePosition() {
    if (impulseOrder){
        impulseMove();
    }
    else{
        rotate();
        sink();
    }
}

void Mussel::impulse() {
    if (!impulseOrder){
        impulseOrder = true;
        setSpriteSheet("mImpulse", 0.1);
        JamEngine::EventManager::addEvent(0, [](int delta){
            JamEngine::Sound::play("jump", 0.2, 0);
        });
    }
}

void Mussel::rotate() {
    int orientation = 0;

    if (rotateR ^ rotateL){
        if (rotateR){
            orientation = -1;
        }
        else{
            orientation = 1;
        }
        // TODO: Bound the possible angles for impulse ?

        angle = angle + orientation*angleDivideFactor;

        while (angle > 360){
            angle -= 360;
        }
        while (angle < 0){
            angle += 360;
        }
    }
}

void Mussel::beginRotateRight() {
    rotateR = true;
}

void Mussel::beginRotateLeft() {
    rotateL = true;
}

void Mussel::endRotateRight() {
    rotateR = false;
}

void Mussel::endRotateLeft() {
    rotateL = false;
}

void Mussel::impulseMove() {
    static int countDown = impulseDivisionNb;
    impulseAllowed = false;

    pos[0] = pos[0] + impulseStrength/impulseDivisionNb * (float) cos((angle+90)*M_PI/180.0);
    pos[1] = pos[1] + impulseStrength/impulseDivisionNb * (float) sin((angle-90)*M_PI/180.0);

    World::world.updatePos(glm::vec2{impulseStrength/impulseDivisionNb * (float) cos((angle+90)*M_PI/180.0),impulseStrength/impulseDivisionNb * (float) sin((angle-90)*M_PI/180.0)});

    if (countDown == 0){
        impulseAllowed = true;
        impulseOrder = false;

        setSpriteSheet("mBase", 0.5);
        countDown = impulseDivisionNb;
    }
    else{
        countDown --;
    }
}

void Mussel::sink() {
    pos[1] = pos[1] + weight*sinkFactor;
    World::world.updatePos(glm::vec2{0,weight*sinkFactor});
}

Player &Mussel::getPlayer()  {
    return player;
}

int Mussel::getFishEggNumber() const {
    return player.getFishEggNumber();
}

bool Mussel::isIsDead() const {
    return isDead;
}

void Mussel::raz() {
	isDead = false;
    pos = glm::vec2{8, 4.5};
    angle = 0.0f;
    impulseAllowed = true;
    impulseOrder = false;
}

void Mussel::incrWeight(float weightIncr) {
    weight= (0.90>=weight+weightIncr>=0.50) ? weight+weightIncr : weight;
}

void Mussel::incrImpulse(float impulseIncr) {
    impulseStrength+=impulseIncr;
}

void Mussel::applyUpgrades() {
    player.upgradeMussel(*this);
}
