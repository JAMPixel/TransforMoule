//
// Created by kelmyn on 18-03-03.
//

#ifndef TRANSFORMOULE_FISHEGGS_H
#define TRANSFORMOULE_FISHEGGS_H


#include <inc/2dEngine/entity.hpp>

class FishEggs : public JamEngine::Entity {
public:
    FishEggs(float posX, float posY);
    ~FishEggs() = default;

    // Jam entity
    void collide(Entity& other) override;
    void update() override;

private:

};


#endif //TRANSFORMOULE_FISHEGGS_H
