//
// Created by kelmyn on 18-03-03.
//

#ifndef TRANSFORMOULE_MUSSEL_H
#define TRANSFORMOULE_MUSSEL_H


#include <inc/2dEngine/entity.hpp>
#include <menu/Player.hpp>

// Player-controlled character
class Mussel : public JamEngine::Entity{
public:
    Mussel();
    ~Mussel();

    Player player{0};
    // Jam Entity
    void collide(Entity& other) override;
    void update() override;

    void incrWeight(float weightIncr);
    void incrImpulse(float impulseIncr);
    void applyUpgrades();

    // Called upon user inputs reception
    void updatePosition();
    void impulse();
    void rotate();

    void beginRotateRight();
    void beginRotateLeft();
    void endRotateRight();
    void endRotateLeft();
    Player &getPlayer() ;
    int getFishEggNumber() const;

private:
    // Weight
    float weight;
    // First weight when beginning the game
    static constexpr float baseWeight = .75;
    static constexpr float sinkFactor = .02;

    // How much (degrees) do you turn when pressing the rotation button ?
    static constexpr float angleDivideFactor = 10;

    // How far can the mussel swim in 1 impulse ?
    float impulseStrength;
    // Base strength
    static constexpr float baseStrength = 2.0f;
    // In how many steps is the impulse divided ?
    static constexpr int impulseDivisionNb = 15;

    // Memorise wether the player is allowed to impulse or not
    bool impulseAllowed;
    // Has the jump order been given ?
    bool impulseOrder;
    // Is the player asking for rotation ?
    bool rotateOrder = false;
    // Has the player asked for rotation to the right ?
    bool rotateR = false;
    // Has the player asked for rotation to the right ?
    bool rotateL = false;
public:
    bool isIsDead() const;
	void raz();

private:
    bool isDead = false;
    // METHODS
    // Position update
    void impulseMove();
    void sink();



};


#endif //TRANSFORMOULE_MUSSEL_H
