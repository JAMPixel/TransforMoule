//
// Created by victor on 18-03-03.
//

#include <world/WorldElement.h>
#include <inc/2dEngine/scene.hpp>
#include "fish/FollowFish.hpp"

WorldElement::WorldElement(int eggs, bool isFish):Entity(0, 0, 0.16, 0.09), eggsAmount(eggs), isFish(isFish){
}

WorldElement::WorldElement(bool isGround):isGround(isGround) {

}
void WorldElement::recreate(glm::vec2 pos, int eggs, bool isFish) {
    for(auto &egg : eggsList){
        JamEngine::scene.destroy(egg);
    }
    eggsList.clear();
    setSpriteSheet("egg", 0.15);
    graphicsSize = {0,0};
    isGround = false;
    this->isFish = isFish;
    if (isFish) {
        bool type = rand()%2;
        if(type)
            JamEngine::scene.add(new FollowFish(pos));
        else
            JamEngine::scene.add(new PatrolFish(pos));

    }
    this->pos = pos;
    eggsAmount = eggs;
    for (int i = 0; i< eggs; ++i){
        float x = rand()/(float)RAND_MAX;
        float y = rand()/(float)RAND_MAX;
        eggsList.push_back(new FishEggs(x+pos.x, y+pos.y));
        JamEngine::scene.add(eggsList[i]);
    }

}

void WorldElement::createGround() {
    isGround = true;
}
void WorldElement::createCleanGround(glm::vec2 pos) {
    //recreate(pos,0,false);return;

    for(auto &egg : eggsList){
        JamEngine::scene.destroy(egg);
    }
    eggsList.clear();
    graphicsSize = {0.5,1};
    isGround = true;
    this->pos = pos;
    eggsAmount = 0;
    isFish = false;
    setSpriteSheet("1", 0.15);

}

WorldElement::~WorldElement() {

}

void WorldElement::collide(JamEngine::Entity &other) {

}

void WorldElement::update() {

}

