//
// Created by victor on 18-03-03.
//

#include <world/world.h>

#include <inc/2dEngine/scene.hpp>
#include <cstdlib>


World World::world;


void World::scrollLeft() {
    xIndexElement = (xIndexElement - 1 +sizeX) % sizeX;
    progression--;
     for (int j = 0; j < sizeY; ++j){

//        if (depth + ((j + yIndexElement) % sizeY) > progression * flatiness) {
//            levelElements[xIndexElement][j].createCleanGround(glm::vec2{levelElements[xIndexElement][j].getPos().x -sizeX,
//                                                                        levelElements[xIndexElement][j].getPos().y});
//        } else {
            int eggsRand = rand() % (progression/20 + eggDensity + depth +j);
            if (eggsRand > eggDensity - 1 + difficulty) {
                eggsRand = std::min(eggsRand - eggDensity - 1 + difficulty,2);
            } else {
                eggsRand = 0;
            }
            bool fish = false;
            if (rand() % std::min(fishNumber ,std::max(1,difficulty * 100 * depth)) > fishDensity) {
                fish = true;
            }
            levelElements[xIndexElement][j].recreate(glm::vec2{levelElements[xIndexElement][j].getPos().x -sizeX,
                                                               levelElements[xIndexElement][j].getPos().y}, eggsRand, fish);
       // }
    }
}

void World::scrollRight() {

    for (int j = 0; j < sizeY; ++j) {

//        if (depth + ((j + yIndexElement) % sizeY) > (sizeX + progression) * flatiness) {
//            levelElements[xIndexElement][j].createCleanGround(glm::vec2{levelElements[xIndexElement][j].getPos().x +sizeX,
//                                                                        levelElements[xIndexElement][j].getPos().y});
//        } else {
            int eggsRand = rand() % (std::max(1,(progression/20 + eggDensity + depth + j)));
            if (eggsRand > eggDensity - 1 + difficulty) {
                eggsRand = std::min(eggsRand - eggDensity - 1 + difficulty,2);
            } else {
                eggsRand = 0;
            }
            bool fish = false;
            if (rand() % std::min(fishNumber ,std::max(1,difficulty * 100 * depth))> fishDensity) {
                fish = true;
            }
            levelElements[xIndexElement][j].recreate(glm::vec2{levelElements[xIndexElement][j].getPos().x +sizeX,
                                                               levelElements[xIndexElement][j].getPos().y},
                                                     eggsRand, fish);
        //}
    }
    progression++;
    xIndexElement = (xIndexElement + 1) % sizeX;
}

void World::scrollUp() {
    yIndexElement = (yIndexElement - 1 + sizeY) % sizeY;
    depth--;

    for (int i = 0; i < sizeX; ++i) {
       /* if (depth > (progression + i) * flatiness) {
            levelElements[i][yIndexElement].createCleanGround(glm::vec2{levelElements[i][yIndexElement].getPos().x,
                                                                        levelElements[i][yIndexElement].getPos().y -
                                                                        sizeY});
        } else {*/
            int eggsRand = rand() % (std::max(1,((progression + i)/20 + eggDensity + depth)));
            if (eggsRand > eggDensity - 1 + difficulty) {
                eggsRand = std::min(eggsRand - eggDensity - 1 + difficulty,2);
            } else {
                eggsRand = 0;
            }
            bool fish = false;
            if (rand() % std::min(fishNumber ,std::max(1,difficulty * 100 * depth)) > fishDensity) {
                fish = true;
            }
            levelElements[i][yIndexElement].recreate(glm::vec2{levelElements[i][yIndexElement].getPos().x,
                                                               levelElements[i][yIndexElement].getPos().y - sizeY},
                                                     eggsRand, fish);
       // }
    }
}

void World::scrollDown() {


    for (int i = 0; i < sizeX; ++i) {
        /*if (depth + sizeY > (progression + i) * flatiness) {
            levelElements[i][yIndexElement].createCleanGround(
                    glm::vec2{levelElements[i][yIndexElement].getPos().x,
                              levelElements[i][yIndexElement].getPos().y +
                              sizeY});
        } else {*/
            int eggsRand = rand() % std::max(1,((progression + i)/20 + eggDensity + depth+sizeY));
            if (eggsRand > eggDensity - 1 + difficulty) {
                eggsRand = std::min(eggsRand - eggDensity - 1 + difficulty,2);
            } else {
                eggsRand = 0;
            }
            bool fish = false;
            if (rand() % std::min(fishNumber ,std::max(1,difficulty * 100 * depth))> fishDensity) {
                fish = true;
            }
            levelElements[i][yIndexElement].recreate(
                    glm::vec2{levelElements[i][yIndexElement].getPos().x, levelElements[i][yIndexElement].getPos().y + sizeY}, eggsRand, fish);
       // }
    }
    depth++;
    yIndexElement = (yIndexElement + 1) % sizeY;

}

World::World() {
    srand(0);
}

void World::init() {
    for (int i = 0; i < sizeX; ++i) {

        for (int j = 0; j < sizeY; ++j) {

          /*  if (depth + j > (progression + i) * flatiness) {
                levelElements[i][j].createCleanGround(glm::vec2(i + 0.5 - (sizeX - 16) / 2, j + 0.5 - (sizeY - 9) / 2));
            } else {*/
                int eggsRand = rand() % std::max(1,((progression + i)/20 + eggDensity+ depth+ j));
                if (eggsRand > eggDensity - 1 + difficulty) {
                    eggsRand = std::min(eggsRand - eggDensity - 1 + difficulty,2);
                } else {
                    eggsRand = 0;
                }
                bool fish = false;
                if (rand() % std::min(fishNumber ,std::max(1,difficulty * 100 * depth)) > fishDensity) {
                    fish = true;
                }
                //WorldElement newElement = WorldElement(eggsRand, fish);
                levelElements[i][j].recreate(glm::vec2(i + 0.5 - (sizeX - 16) / 2, j + 0.5 - (sizeY - 9) / 2), eggsRand, fish);
            //}
            JamEngine::scene.add(&levelElements[i][j]);
        }
    }
}

void World::updatePos(glm::vec2 addingPos) {
    musselPos += addingPos;

    if(musselPos.x < 6){
        scrollLeft();
        musselPos.x++;
    } else if(musselPos.x > 10){
        scrollRight();
        musselPos.x--;
    }
    if(musselPos.y < 3.5){
        scrollUp();
        musselPos.y++;
    } else if(musselPos.y > 5.5){
        scrollDown();
        musselPos.y--;
    }

}