//
// Created by victor on 18-03-03.
//

#ifndef TRANSFORMOULE_WORLD_H
#define TRANSFORMOULE_WORLD_H


#include <vector>
#include "WorldElement.h"

class World
{
private:
    constexpr static int sizeX = 30;
    constexpr static int sizeY = 15;
    int depth = -3;
    int progression = 2;
    int flatiness =2; // could be float for quick depth
    int difficulty =1;// >=1
    int eggDensity =1000;
    int fishDensity = 992 ;
    int fishNumber = 1000 ;
    int xIndexElement = 0;
    int yIndexElement  = 0;
    glm::vec2 musselPos{8,4.5};

    std::array<std::array<WorldElement, sizeY>, sizeX> levelElements; //on pourrait mettre des array

    World();

public:
    void scrollLeft();
    void scrollRight();
    void scrollUp();
    void scrollDown();
    static World world;
    void init();
    void updatePos(glm::vec2 addingPos);
};
#endif //TRANSFORMOULE_WORLD_H
