//
// Created by victor on 18-03-03.
//

#ifndef TRANSFORMOULE_WORLDELEMENT_H
#define TRANSFORMOULE_WORLDELEMENT_H

#include <vector>
#include "entity.hpp"
#include "entities/FishEggs.h"
class WorldElement: public JamEngine::Entity {

    bool isGround = false;
    int eggsAmount = 0;
    bool isFish = false;
    //Fish *;
    std::vector<FishEggs *> eggsList;

public:
    WorldElement(int eggs = 0, bool isFish = false);

    WorldElement(bool isGround);

    void recreate(glm::vec2 pos, int eggs, bool isFish);

    void createGround();
    void createCleanGround(glm::vec2 pos);
    void collide(Entity &other);
    void update();
    virtual ~WorldElement();
};


#endif //TRANSFORMOULE_WORLDELEMENT_H
