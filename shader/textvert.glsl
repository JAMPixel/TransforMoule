#version 330 core
layout(location = 0) in vec3 vertices;
layout (location = 1)in vec2 uv;

uniform mat3 scale;
uniform mat3 transformation;
uniform vec2 bd;
uniform vec2 hg;

out vec2 uvOut;

void main() {

    gl_Position = vec4(scale*transformation*vertices,1.);
    uvOut = mix(hg,hg+bd,uv);
}
