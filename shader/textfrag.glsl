#version 330 core
in vec2 uvOut;
out vec4 colors;
uniform sampler2D texture2D;
uniform vec3 color;

void main() {
    colors = texture(texture2D,uvOut).rgba;
}

