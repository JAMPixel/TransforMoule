/*!
 * @file Player.hpp
 * @author Justin Goutey.
 * @date 03 mars 2018.
 * @brief
 * @addtogroup
 */

#ifndef TRANSFORMOULE_PLAYER_HPP
#define TRANSFORMOULE_PLAYER_HPP



// Standards Includes

// Libraries Includes

// Local Includes

#include <menu/MarketItem.hpp>
#include <vector>

class Mussel;

/**
 * @class Player
 */

class Player {
private:
    int _fishEggCount;
    std::vector<MarketItem> _marketItems;
public:
    Player(int fishEggCount);

	void addFishEgg();
    void buy(const MarketItem & marketItem);
	int getFishEggNumber() const;
	void upgradeMussel(Mussel & mussel);
};

#endif //TRANSFORMOULE_PLAYER_HPP
