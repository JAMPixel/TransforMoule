/*!
 * @file BuyableEntry.cpp
 * @author Justin Goutey.
 * @date 03 mars 2018.
 * @brief
 * @addtogroup
 */

#include "BuyableEntry.hpp"

BuyableEntry::BuyableEntry(const std::string &spriteKey, const glm::vec2 &initialPosition, const MarketItem &marketItem) :
        MenuEntry(spriteKey, initialPosition),
        _marketItem(marketItem)
{

}

void BuyableEntry::validate(Player &player) {
    player.buy(_marketItem);
}
