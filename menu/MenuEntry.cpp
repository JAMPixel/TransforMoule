/*!
 * @file MenuEntry.cpp
 * @author Justin Goutey.
 * @date 03 mars 2018.
 * @brief
 * @addtogroup
 */

#include <inc/2dEngine/spriteSheetManager.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <menu/MenuEntry.hpp>

MenuEntry::MenuEntry(const std::string & spriteKey, const glm::vec2 & initialPosition, const glm::vec2 & size) :
        Entity(initialPosition.x, initialPosition.y, 1, 1)
{
    pos = initialPosition;
    layer = "menu_entry";
    setSpriteSheet(spriteKey, 0.5f);
}

void MenuEntry::collide(JamEngine::Entity &other) {

}

void MenuEntry::update() {

}
