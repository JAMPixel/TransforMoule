/*!
 * @file BuyMenu.hpp
 * @author Justin Goutey.
 * @date 03 mars 2018.
 * @brief
 * @addtogroup
 */

#ifndef TRANSFORMOULE_BUYMENU_HPP
#define TRANSFORMOULE_BUYMENU_HPP



// Standards Includes

// Libraries Includes

// Local Includes
#include <menu/Player.hpp>
#include <menu/MenuEntry.hpp>
#include <entities/Mussel.h>
#include "Menu.hpp"

/**
 * @class BuyMenu
 */

class BuyMenu: public Menu {
private:
    std::string previousContextName;
    int focusedIndex;
    MenuEntry * exitButton;
    Player * _player;
	Mussel * mussel;
public:
    BuyMenu(Mussel *mussel);

    void collide(Entity &other) override;

	void display(float delta) override;

	void update() override;

    Entity * select() override;

    void move(Direction direction) override;

    void enter(Player & player);
    void exit();
};


#endif //TRANSFORMOULE_BUYMENU_HPP
