/*!
 * @file MarketItem.hpp
 * @author Justin Goutey.
 * @date 03 mars 2018.
 * @brief
 * @addtogroup
 */

#ifndef TRANSFORMOULE_MARKETITEM_HPP
#define TRANSFORMOULE_MARKETITEM_HPP



// Standards Includes
#include <string>
#include <functional>

// Libraries Includes

// Local Includes

class Mussel;

/**
 * @class MarketItem
 */

class MarketItem {
private:
    std::string _name;
    int _cost;
    std::function<void(Mussel &)> _musselModifier;
public:
    MarketItem(std::string &&name, int cost, std::function<void(Mussel &)> &&musselModifier);
    MarketItem(const MarketItem & marketItem) = default;
    int cost() const;
    void upgrade(Mussel & mussel);
};


#endif //TRANSFORMOULE_MARKETITEM_HPP
