/*!
 * @file MarketItem.cpp
 * @author Justin Goutey.
 * @date 03 mars 2018.
 * @brief
 * @addtogroup
 */

#include <string>
#include "MarketItem.hpp"

MarketItem::MarketItem(std::string &&name, int cost, std::function<void(Mussel &)> &&musselModifier) :
        _name(std::move(name)),
        _cost(cost),
        _musselModifier(std::move(musselModifier))
{

}

int MarketItem::cost() const {
    return _cost;
}

void MarketItem::upgrade(Mussel &mussel) {
    _musselModifier(mussel);
}
