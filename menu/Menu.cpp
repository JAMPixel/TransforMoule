/*!
 * @file Menu.cpp
 * @author Justin Goutey.
 * @date 03 mars 2018.
 * @brief
 * @addtogroup
 */

#include <menu/Menu.hpp>

Menu::Menu() : Entity(8, 4.5, 16, 9)
{

}

Menu::FocusedItem::FocusedItem(const std::string &spriteKey) :
        Entity(0, 0, 1.1, 1.1)
{
    setSpriteSheet(spriteKey, 0.01);
    layer = "menu_focus";
}

void Menu::FocusedItem::focusOn(MenuEntry *entity) {
    focusedItem = entity;
}

JamEngine::Entity *Menu::FocusedItem::getFocusedItem() {
    return focusedItem;
}

void Menu::FocusedItem::collide(JamEngine::Entity &other) {

}

void Menu::FocusedItem::update() {
    pos = glm::vec2(focusedItem->getPosX(), focusedItem->getPosY());
}

void Menu::FocusedItem::validate(Player & player) {
    focusedItem->validate(player);
}
