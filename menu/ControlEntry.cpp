/*!
 * @file ControlEntry.cpp
 * @author Justin Goutey.
 * @date 03 mars 2018.
 * @brief
 * @addtogroup
 */

#include "ControlEntry.hpp"

ControlEntry::ControlEntry(const std::string &spriteKey, const glm::vec2 &initialPosition, const std::function<void(Player &)> &validationFunction) :
        MenuEntry(spriteKey, initialPosition),
        _validationFunction(validationFunction)
{

}

void ControlEntry::validate(Player &player) {
    _validationFunction(player);
}
