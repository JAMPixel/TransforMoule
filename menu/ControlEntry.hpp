/*!
 * @file ControlEntry.hpp
 * @author Justin Goutey.
 * @date 03 mars 2018.
 * @brief
 * @addtogroup
 */

#ifndef TRANSFORMOULE_CONTROLENTRY_HPP
#define TRANSFORMOULE_CONTROLENTRY_HPP



// Standards Includes

// Libraries Includes

// Local Includes
#include "MenuEntry.hpp"

/**
 * @class ControlEntry
 */

class ControlEntry: public MenuEntry {
private:
    std::function<void(Player &)> _validationFunction;
public:
    ControlEntry(const std::string &spriteKey, const glm::vec2 &initialPosition, const std::function<void(Player &)> &validationFunction);
    void validate(Player &player);
};


#endif //TRANSFORMOULE_CONTROLENTRY_HPP
