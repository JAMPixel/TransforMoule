/*!
 * @file Player.cpp
 * @author Justin Goutey.
 * @date 03 mars 2018.
 * @brief
 * @addtogroup
 */

#include <iostream>
#include "Player.hpp"

Player::Player(int fishEggCount) :
    _fishEggCount(fishEggCount)
{

}

void Player::buy(const MarketItem &marketItem) {
    if(_fishEggCount >= marketItem.cost()) {
        _fishEggCount -= marketItem.cost();
        _marketItems.push_back(marketItem);
    }
}

void Player::addFishEgg() {
    _fishEggCount++;

}

int Player::getFishEggNumber() const {
	return _fishEggCount;
}

void Player::upgradeMussel(Mussel &mussel) {
    for (MarketItem & marketItem: _marketItems) {
        marketItem.upgrade(mussel);
    }
    _marketItems.clear();
}
