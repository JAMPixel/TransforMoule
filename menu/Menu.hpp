/*!
 * @file Menu.hpp
 * @author Justin Goutey.
 * @date 03 mars 2018.
 * @brief
 * @addtogroup
 */

#ifndef TRANSFORMOULE_MENU_HPP
#define TRANSFORMOULE_MENU_HPP



// Standards Includes
#include <vector>

// Libraries Includes
#include <inc/2dEngine/entity.hpp>

// Local Includes
#include "MenuEntry.hpp"
#include "Player.hpp"




/**
 * @class Menu
 */

class Menu: public JamEngine::Entity {
protected:
    class FocusedItem: public Entity {
    private:
        MenuEntry * focusedItem;
    public:
        FocusedItem(const std::string & spriteKey);

        void focusOn(MenuEntry *entity);

        Entity * getFocusedItem();

        void collide(Entity &other) override;

        void update() override;

        void validate(Player & player);
    };

    std::vector<MenuEntry *> entries;
    FocusedItem * focusedItem;
public:

    enum Direction {
        RIGHT, LEFT, UP, DOWN
    };

    Menu();

    virtual Entity * select() = 0;
    virtual void move(Direction) = 0;
};


#endif //TRANSFORMOULE_MENU_HPP
