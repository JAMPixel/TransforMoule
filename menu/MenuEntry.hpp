/*!
 * @file MenuEntry.hpp
 * @author Justin Goutey.
 * @date 03 mars 2018.
 * @brief
 * @addtogroup
 */

#ifndef TRANSFORMOULE_MENUENTRY_HPP
#define TRANSFORMOULE_MENUENTRY_HPP



// Standards Includes
#include <functional>

// Libraries Includes
#include <inc/2dEngine/entity.hpp>
#include <inc/2dEngine/sprite.hpp>

// Local Includes
#include "Player.hpp"


/**
 * @class MenuEntry
 */

class MenuEntry: public JamEngine::Entity {
public:
    MenuEntry(const std::string & spriteKey, const glm::vec2 & initialPosition, const glm::vec2 & size = {1, 1});


    void collide(Entity &other) override;

    void update() override;

    virtual void validate(Player & player) = 0;
};


#endif //TRANSFORMOULE_MENUENTRY_HPP
