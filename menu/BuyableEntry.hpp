/*!
 * @file BuyableEntry.hpp
 * @author Justin Goutey.
 * @date 03 mars 2018.
 * @brief
 * @addtogroup
 */

#ifndef TRANSFORMOULE_BUYABLEENTRY_HPP
#define TRANSFORMOULE_BUYABLEENTRY_HPP



// Standards Includes

// Libraries Includes

// Local Includes
#include "MenuEntry.hpp"

/**
 * @class BuyableEntry
 */

class BuyableEntry: public MenuEntry {
private:
    MarketItem _marketItem;
public:
    BuyableEntry(const std::string &spriteKey, const glm::vec2 &initialPosition, const MarketItem &marketItem);

    void validate(Player &player);
};


#endif //TRANSFORMOULE_BUYABLEENTRY_HPP
