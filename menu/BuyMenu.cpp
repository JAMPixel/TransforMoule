/*!
 * @file BuyMenu.cpp
 * @author Justin Goutey.
 * @date 03 mars 2018.
 * @brief
 * @addtogroup
 */

#include <inc/2dEngine/scene.hpp>
#include <inc/2dEngine/keyEventHandler.hpp>
#include <inc/2dEngine/programManager.hpp>
#include <world/world.h>
#include <text/displayText.hpp>
#include "BuyMenu.hpp"
#include "BuyableEntry.hpp"
#include "ControlEntry.hpp"

BuyMenu::BuyMenu(Mussel *mussel) :
        Menu(),
        focusedIndex(0),
        _player(nullptr),
        previousContextName(JamEngine::KeyEventHandler::getContextName()),
        mussel(mussel)
{
    layer = "menu";
    setSpriteSheet("menu_background", 1);

    entries.push_back(new BuyableEntry("lightweight", glm::vec2(3, 4.5), MarketItem("Item 1", 1, [](Mussel & mussel){
        mussel.incrWeight(-0.05f);
    })));
    entries.push_back(new BuyableEntry("impulse", glm::vec2(8, 4.5), MarketItem("Item 2", 3, [](Mussel & mussel){
        mussel.incrImpulse(0.1f);
    })));
    entries.push_back(new BuyableEntry("super_impulse", glm::vec2(13, 4.5), MarketItem("Item 3", 25, [](Mussel & mussel){
        mussel.incrImpulse(1);
    })));
    exitButton = new ControlEntry("exit", glm::vec2(14.5, 7.5), [this](Player & player){
        exit();
    });
    focusedItem = new Menu::FocusedItem("menu_focus");

    JamEngine::KeyEventHandler::selectContext("buyMenu.ini");

    focusedItem->focusOn(entries[0]);JamEngine::KeyEventHandler::addKeyEventPressed("menuRight", [this]{
        move(Direction::RIGHT);
    });
    JamEngine::KeyEventHandler::addKeyEventPressed("menuLeft", [this]{
        move(Direction::LEFT);
    });
    JamEngine::KeyEventHandler::addKeyEventPressed("menuUp", [this]{
        move(Direction::UP);
    });
    JamEngine::KeyEventHandler::addKeyEventPressed("menuDown", [this]{
        move(Direction::DOWN);
    });
    JamEngine::KeyEventHandler::addKeyEventPressed("menuValidate", [this]{

        focusedItem->validate(*_player);
    });

    JamEngine::KeyEventHandler::selectContext(previousContextName);
}


void BuyMenu::collide(JamEngine::Entity &other) {

}

void BuyMenu::update() {
    pos = JamEngine::ProgramManager::getPosCam();
}

JamEngine::Entity * BuyMenu::select() {
    return focusedItem->getFocusedItem();
}

void BuyMenu::move(Menu::Direction direction) {
    switch (direction) {
        case Menu::Direction::RIGHT:
            if(focusedIndex != -1) {
                focusedIndex = (focusedIndex == int(entries.size())-1) ? 0 : focusedIndex+1;
                focusedItem->focusOn(entries[focusedIndex]);
            }
            break;
        case Menu::Direction::LEFT:
            if (focusedIndex != -1) {
                focusedIndex = (focusedIndex == 0) ? int(entries.size())-1 : focusedIndex-1;
                focusedItem->focusOn(entries[focusedIndex]);
            }
            break;
        case Menu::Direction::UP:
            if(focusedIndex == -1) {
                focusedIndex = 0;
                focusedItem->focusOn(entries[focusedIndex]);
            }
            break;
        case Menu::Direction::DOWN:
            if(focusedIndex != -1) {
                focusedIndex = -1;
                focusedItem->focusOn(exitButton);
            }
            break;
    }
}

void BuyMenu::enter(Player &player) {
    _player = &player;
    for (auto menuEntry: entries) {
        JamEngine::scene.add(menuEntry);
    }
    JamEngine::scene.add(this);
    JamEngine::scene.add(focusedItem);
    JamEngine::scene.add(exitButton);

    previousContextName = JamEngine::KeyEventHandler::getContextName();
    JamEngine::KeyEventHandler::selectContext("buyMenu.ini");
}

void BuyMenu::exit() {
    for (auto menuEntry: entries) {
        JamEngine::scene.remove(menuEntry);
    }
    JamEngine::scene.remove(focusedItem);
    JamEngine::scene.remove(exitButton);
    JamEngine::scene.remove(this);
    JamEngine::scene.clearNoDelete();
    _player = nullptr;
    JamEngine::KeyEventHandler::selectContext(previousContextName);
    JamEngine::scene.add(mussel);
    World::world.init();
    mussel->applyUpgrades();
}

void BuyMenu::display(float delta) {
    Entity::display(delta);
    displayText.display(std::to_string(_player->getFishEggNumber()), {25, 25}, {50, 50});
}
