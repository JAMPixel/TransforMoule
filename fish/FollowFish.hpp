//
// Created by stiven on 18-03-03.
//

#ifndef TRANSFORMOULE_FOLLOWFISH_HPP
#define TRANSFORMOULE_FOLLOWFISH_HPP


#include "patrolFish.hpp"

class FollowFish : public PatrolFish{

	glm::vec2 posToFollow;
	float speed = 0.05;
	float radius = 5;
	bool follow = false;
public:
	FollowFish(glm::vec2 const & pos);

	void update() override;
	void collide(Entity &other) override;
};


#endif //TRANSFORMOULE_FOLLOWFISH_HPP
