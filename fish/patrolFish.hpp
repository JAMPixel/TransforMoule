//
// Created by stiven on 18-03-03.
//

#ifndef TRANSFORMOULE_PATROLFISH_HPP
#define TRANSFORMOULE_PATROLFISH_HPP


#include "fish.hpp"

class PatrolFish : public Fish{

protected:
	float firstPos;
	float deltaMovement = 0.01;
	float maxDeplace  = 1.5;
	int coef = 1;


public:
	PatrolFish(glm::vec2 const & pos);
	void update()override;
};


#endif //TRANSFORMOULE_PATROLFISH_HPP
