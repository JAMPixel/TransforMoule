//
// Created by stiven on 18-03-03.
//

#ifndef TRANSFORMOULE_FISH_HPP
#define TRANSFORMOULE_FISH_HPP


#include <inc/2dEngine/entity.hpp>

class Fish : public JamEngine::Entity {


public:
	using Entity::Entity;
	void update() override ;
	void collide(Entity & entity) override;
};


#endif //TRANSFORMOULE_FISH_HPP
