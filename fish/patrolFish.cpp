//
// Created by stiven on 18-03-03.
//

#include "patrolFish.hpp"

void PatrolFish::update() {

	pos.x += coef*deltaMovement;
	if(abs(pos.x - firstPos) >= maxDeplace){
		coef *= -1;
 		graphicsSize.x*= -1;
	}
}
PatrolFish::PatrolFish(glm::vec2 const &pos):Fish(pos), firstPos(pos.x) {
	setSpriteSheet("patrolFish", 0.1);
}


