//
// Created by stiven on 18-03-03.
//

#include <entities/Mussel.h>
#include <glm/geometric.hpp>
#include "FollowFish.hpp"

FollowFish::FollowFish(glm::vec2 const &pos) : PatrolFish(pos) {
	setSpriteSheet("followFish", 0.1);
}

void FollowFish::update() {
	if(!follow) {
		PatrolFish::update();
		return;
	}
	glm::vec2 dir = posToFollow - pos;
	dir = glm::normalize(dir+0.001f);
	if(dir.x < 0){
		graphicsSize.x = -size.x;
	}else{
		graphicsSize.x = size.x;
	}
	pos += speed*dir;
}

void FollowFish::collide(JamEngine::Entity &other) {
	PatrolFish::collide(other);
	if(!dynamic_cast<Mussel *>(&other)){
		return;
	}
	if(glm::length(pos - glm::vec2(other.getPosX(), other.getPosY())) < radius){
		follow = true;
	}
	if(follow){
		posToFollow.x = other.getPosX();
		posToFollow.y = other.getPosY();
	}
	if(glm::length(pos - glm::vec2(other.getPosX(), other.getPosY())) > 2*radius){
		follow = false;
		firstPos = pos.x;
	}
}
