//
// Created by stiven on 18-03-03.
//

#include <glm/geometric.hpp>
#include <entities/Mussel.h>
#include <inc/2dEngine/scene.hpp>
#include "fish.hpp"

void Fish::update() {


}

void Fish::collide(JamEngine::Entity &other) {

	if(!dynamic_cast<Mussel *>(&other)){
		return;
	}
	if(glm::length(pos - glm::vec2(other.getPosX(), other.getPosY())) > 20){
		JamEngine::scene.destroy(this);
	}
}
