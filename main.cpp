#include <2dEngine/windowSettings.hpp>
#include <2dEngine/window.hpp>
#include <2dEngine/gameState.hpp>
#include <inc/2dEngine/scene.hpp>
#include <inc/2dEngine/spriteSheetManager.hpp>
#include <inc/2dEngine/programManager.hpp>
#include <inc/2dEngine/shapeManager.hpp>
#include <menu/BuyMenu.hpp>
#include <inc/2dEngine/keyEventHandler.hpp>
#include <entities/FishEggs.h>
#include "entities/Mussel.h"
#include <fish/patrolFish.hpp>
#include <inc/2dEngine/sound.hpp>
#include <inc/2dEngine/eventManager.hpp>
#include <fish/FollowFish.hpp>
#include <world/world.h>
#include <text/displayText.hpp>

using namespace JamEngine;

struct Foo : Entity{

	Foo():Entity(){
		pos.x = 10.0f;
		pos.y = 100.0;
		size.x = 10;
		size.y = 10;

	}
	void collide (Entity &)override{}
	void update()override{}
};

void raz(){
	scene.clear();
	ProgramManager::raz();

}

int main(){


	windowSettings settings{"TransforMoule", 1366, 768};
	Window window{settings};
	SpriteSheetManager::init("../configFile/texture.ini");
	KeyEventHandler::init("../configFile/key.ini", "../configFile/key/buyMenu.ini");
	KeyEventHandler::selectContext("key.ini");
	scene.init("../configFile/layer.ini");
	Sound::init("../configFile/sound.ini");
	ShapeManager::init();
	ProgramManager::init(settings);
	Sound::playInfinite("generique", 0.3);
	displayText.init();

    World::world.init();

    //scene.add(new FishEggs(2, 2));
	//scene.add(new FollowFish(glm::vec2(8, 4.5)));
	Mussel * m = new Mussel();
	BuyMenu menu(m);
	scene.add(m);
	//menu.enter(m->getPlayer());
	EventManager::addEvent(-1, [&m, &menu](int i){
		if(m) {
			if (m->isIsDead()) {
				scene.remove(m);
				raz();
 				menu.enter(m->getPlayer());
				m->raz();
			}
		}
	});
	KeyEventHandler::addKeyEventPressed("hitbox", []{
		GameState::debug();
		moveCam(0, -1);
	});

	GameState::loop(window);
}