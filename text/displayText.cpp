//
// Created by stiven on 18/03/16.
//

#include "displayText.hpp"
#include <fstream>
#include <string>
#include <iostream>

Name displayText;
void createVertexShader(std::string path) {
	std::ifstream fileTest(path);
	if (!fileTest.fail()) {
		std::cout << "le fichier " << path << " existe déjà " << std::endl;
		return;
	}
	std::ofstream file(path, std::ios::ate);
	file << R"lol(#version 330 core
layout(location = 0) in vec3 vertices;
layout (location = 1)in vec2 uv;

uniform mat3 scale;
uniform mat3 transformation;
uniform vec2 bd;
uniform vec2 hg;

out vec2 uvOut;

void main() {

    gl_Position = vec4(scale*transformation*vertices,1.);
    uvOut = mix(hg,hg+bd,uv);
}
)lol";
	std::cout << "le fichier a été créé à cet endroit " << path << std::endl;
}

void createFragmentShader(std::string path) {
	std::ifstream fileTest(path);
	if (!fileTest.fail()) {
		std::cout << "le fichier " << path << " existe déjà " << std::endl;
		return;
	}
	std::ofstream file(path, std::ios::ate);
	file << R"lol(#version 330 core
in vec2 uvOut;
out vec4 colors;
uniform sampler2D texture2D;
uniform vec3 color;

void main() {
    colors = texture(texture2D,uvOut).rgba;
}

)lol";
	std::cout << "le fichier a été créé à cet endroit " << path << std::endl;
}

void createShaders(std::string vertex, std::string fragment) {
	createVertexShader(vertex);
	createFragmentShader(fragment);
}
