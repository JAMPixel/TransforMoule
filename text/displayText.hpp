//
// Created by stiven on 14/02/16.
//
#ifndef OPENGL_DISPLAYTEXT_HPP
#define OPENGL_DISPLAYTEXT_HPP

#include <string>
#include <glm/vec2.hpp>
#include <glish/inc/glish/Vao.hpp>
#include <glish/inc/glish/utils/ProgramGL.hpp>
#include <glish/inc/glish/Texture.hpp>
#include <2dEngine/programManager.hpp>

void createVertexShader(std::string path);

void createFragmentShader(std::string path);

void createShaders(std::string vertex, std::string fragment);

template<typename T>
void display(T &glText, std::string text, glm::vec2 pos, glm::vec2 size);

template<typename T>
void init(T &glText, glm::vec2 scale);

inline const std::vector<glm::vec3> rect
		{
				{0.0f, 0.0f, 1.0f},
				{0.0f, 1.0f, 1.0f},
				{1.0f, 0.0f, 1.0f},
				{1.0f, 1.0f, 1.0f}
		};
inline const std::vector<glm::vec2> uv
		{
				{0.0f, 0.0f},
				{0.0f, 1.0f},
				{1.0f, 0.0f},
				{1.0f, 1.0f}
		};
struct TextInit {
	TextInit() { createShaders("../shader/textvert.glsl", "../shader/textfrag.glsl"); }
};
enum class uniText{
	hg, bd, scale, transformation, texture2D, color
};
struct Name : public TextInit {
	glish::Vao<2> *vao = nullptr;
	using progText = glish::ProgramGL<uniText, glm::vec2, glm::vec2, glm::mat3, glm::mat3, int, glm::vec3>;
	progText prog;
	glish::Texture texture;

	Name() {

	}
	void init(){
		vao = new glish::Vao<2>;
		vao->addVbo(0, rect);
		vao->addVbo(1, uv);
		prog = progText{glish::shaderFile{GL_VERTEX_SHADER, "../shader/textvert.glsl"},
		                glish::shaderFile{GL_FRAGMENT_SHADER, "../shader/textfrag.glsl"}};
		prog.init("hg", "bd", "scale", "transformation", "texture2D", "color");
		texture = glish::Texture{"../text/font.png"};
		prog.use();
		prog.set<uniText::color>( glm::vec3(0.0f, 0.0f, 0.0f));
		prog.set<uniText::bd>( glm::vec2(1 / 16.0f, 1 / 16.0f));
		prog.set<uniText::scale>( JamEngine::normalizedScreenSpace(1366, 768));
	}
	void display(std::string text, glm::vec2 pos, glm::vec2 size) {
		vao->bind();
		prog.use();
		JamEngine::transformStorage transformation;
		transformation.tran = pos;
		transformation.scale = size;
		texture.bindTexture(0);
		prog.set<uniText::texture2D>( 0);
		size_t taille = text.size();
		for (size_t i = 0; i < taille; i++) {
			prog.set<uniText::hg>(glm::vec2((float) (text[i] % (16)) / 16, (float) ((std::div(text[i], 16).quot)) / 16));
			prog.set<uniText::transformation>( JamEngine::transform(transformation, 0.0f, 0.0f));
			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
			// gère le retour à la ligne
			if (text[i] == '\n') {
				transformation.tran.y += size.y / 2;
				transformation.tran.x = pos.x;
			} else {
				transformation.tran.x += size.x / 2;
			}

		}
	}
	~Name(){
		delete  vao;
	}
};

extern Name displayText;


#include "displayText.tpp"

#endif //OPENGL_DISPLAYTEXT_HPP
