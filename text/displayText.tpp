#include "displayText.hpp"
#include <cstdlib>
#include <inc/2dEngine/programManager.hpp>


template<typename T>
void display(T &glText, std::string text, glm::vec2 pos, glm::vec2 size) {
	glText.vao->bind();
	JamEngine::transformStorage transformation;
	transformation.tran = pos;
	transformation.scale = size;
	glText.texture.bindTexture();
	glText.prog.set<uniText::texture2D>( 0);
	size_t taille = text.size();
	for (size_t i = 0; i < taille; i++) {
		glText.prog.set<uniText::hg>(glm::vec2((float) (text[i] % (16)) / 16, (float) ((std::div(text[i], 16).quot)) / 16));
		glText.prog.set<uniText::transformation>( JamEngine::transform(transformation, 0.0f, 0.0f));
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		// gère le retour à la ligne
		if (text[i] == '\n') {
			transformation.tran.y += size.y / 2;
			transformation.tran.x = pos.x;
		} else {
			transformation.tran.x += size.x / 2;
		}


	}
}

template<typename T>
void init(T &glText, glm::vec2 scale) {

	glText.prog.set<uniText::color>( glm::vec3(0.0f, 0.0f, 0.0f));
	glText.prog.set<uniText::bd>( glm::vec2(1 / 16.0f, 1 / 16.0f));
	glText.prog.set<uniText::scale>( JamEngine::normalizedScreenSpace(scale.x, scale.y));

}

