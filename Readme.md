#TransforMoule

##Utilisation de JamEngine


####Classe importante :

Entity : cette classe contient 2 fonctions
update() : override pour definie le comportement de l'entite  a chaque frame
collide(Entity &) : override pour definir le comportement de collision, a noter : la detection de collision doit etre coder


Quand une entite est cree, elle doit etre ajouter a la scene via :
scene.add(Entity *)

une fois ajouter les fonctions seront exeute a chaque frame

KeyEvent :
un fichier de config doit etre defini comme suit:

exemple :
"move = w, z"

on defini le fichier de config
KeyEventHandler::init(file)

puis pour definir une touche :
lorsqu'on appuie

KeyEventHandler::addKeyEventPressed("move", []{});

lorsqu'on relache

KeyEventHandler::addKeyEventRelease("move", []{});

Pour definir un evenement
EventManager::addEvent(3, []{}):
3 correspond au temps en seconde d'execution


