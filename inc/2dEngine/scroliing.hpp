//
// Created by stiven on 18-03-03.
//

#ifndef TRANSFORMOULE_SCROLIING_HPP
#define TRANSFORMOULE_SCROLIING_HPP


#include "sprite.hpp"
using namespace JamEngine;
class Scrolling {
	glm::vec2 size;
	glm::vec2 orig;

	SpriteSheet * spriteSheet = nullptr;

	void fixOneVariable(float & a);
	void fixOrig();
public:
	Scrolling() = default;
	Scrolling(SpriteSheet * spriteSheet, glm::vec2 const &orig);

	void scroll(float x, float y);

	info getInfo() const;
	void use() ;


};



#endif //TRANSFORMOULE_SCROLIING_HPP
