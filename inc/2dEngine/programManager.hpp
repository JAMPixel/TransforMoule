//
// Created by stiven on 18-03-03.
//

#ifndef TRANSFORMOULE_PROGRAMMANAGER_HPP
#define TRANSFORMOULE_PROGRAMMANAGER_HPP

#include <glish/program/Program.hpp>
#include <glish/utils/ProgramGL.hpp>
#include "windowSettings.hpp"
#include "scroliing.hpp"

namespace JamEngine {
	struct transformStorage {
		glm::vec2 tran = glm::vec2(0.0f);
		glm::vec2 scale = glm::vec2(1.0f);
		float angle = 0.0f;
	};

	glm::mat3 transform(transformStorage const &store, float offsetX = 0,
	                    float offsetY = 0);
	template<typename T>
	glm::mat3 normalizedScreenSpace(const T width, const T height) {
		glm::mat3 mat(
				2.0f / width, 0.0f, 0.f,
				0.0f, -2.0f / height, 0.0f,
				-1.0f, 1.0f, 1.0f
		);

		return mat;
	}
	void moveCam(float x, float y);
	class ProgramManager {
		enum class uni{
			texture, scale, orig, textureSize, transform
		};
		using Program = glish::ProgramGL<uni, int, glm::mat3, glm::vec2, glm::vec2, glm::mat3>;
		Program program;

		glm::vec2 abstractSizeScreen;
		glm::vec2 cam;
		static ProgramManager programManager;

		Scrolling scrolling;
	public :
		static void init(const windowSettings &settings);
		static void
		use(const glm::vec2 &orig, const glm::vec2 &textureSize, glm::vec2 const &size, glm::vec2 const &pos, float angle);
		static void moveCam(float x, float y);

		static void displayBackground();
		static glm::vec2 const & getPosCam();
		static void raz();
	};
}


#endif //TRANSFORMOULE_PROGRAMMANAGER_HPP
