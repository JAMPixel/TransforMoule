//
// Created by stiven on 18-03-02.
//

#ifndef INC_2DENGINE_SPRITE_HPP
#define INC_2DENGINE_SPRITE_HPP

#include "spriteSheet.hpp"

namespace JamEngine {
	struct info{
		glm::vec2 orig;
		glm::vec2 size;
	};
	class Sprite {

		SpriteSheet * spriteSheet = nullptr ;
		int nbImage = 0;
		float timeChange = 0;
		float currentTime = 0;
	public:
		Sprite() = default;
		Sprite(SpriteSheet *spriteSheet, float timeChange, int nbImage = 0);
		info update(float delta);
	};
}


#endif //INC_2DENGINE_SPRITE_HPP
