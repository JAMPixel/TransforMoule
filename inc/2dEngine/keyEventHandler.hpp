//
// Created by stiven on 18-01-20.
//

#ifndef ALLPROJECT_KEYEVENTHANDLER_HPP
#define ALLPROJECT_KEYEVENTHANDLER_HPP

#include <string>
#include <functional>
#include <map>
#include <2dEngine/keyHandler.hpp>
#include <utils/stringUtil.h>

namespace JamEngine {
	struct KeyEventHandler {

		using mapFunction = std::map<std::string, std::vector<std::function<void()>>>;
		using eventFunction = std::function <void()>;
	private:
		KeyHandler keyHandler;
		mapFunction functionPressed;
		mapFunction functionReleased;

		static std::string contextName;
		static KeyEventHandler * eventHandler;
		static std::map<std::string, KeyEventHandler *> eventHandlers;
		KeyEventHandler() = default;

		void executeFunction(int key, mapFunction &map);
	public:
		static void init(std::string && configFile) {
			auto * keyEventHandler = new KeyEventHandler();
			std::string newContextName = utils::extractFile(configFile);
			keyEventHandler->keyHandler.mapKey(std::move(configFile));

			eventHandlers[newContextName] = keyEventHandler;
		}

		template <class ...Ts>
		static void init(std::string && configFile, Ts && ... args) {
			auto * keyEventHandler = new KeyEventHandler();
			std::string newContextName = utils::extractFile(configFile);
			keyEventHandler->keyHandler.mapKey(std::move(configFile));

			eventHandlers[newContextName] = keyEventHandler;

			init(std::forward<Ts>(args)...);
		};

		static void selectContext(const std::string & contextName);
		static const std::string & getContextName();
		static void addKeyEventPressed(std::string &&key, eventFunction function);

		static void executePressedFunction(int key);
		static void addKeyEventReleased(std::string &&key, eventFunction function);

		static void executeReleasedFunction(int key);
	};

}
#endif //ALLPROJECT_KEYEVENTHANDLER_HPP
