//
// Created by stiven on 18-03-03.
//

#include "scroliing.hpp"

Scrolling::Scrolling(SpriteSheet *spriteSheet, glm::vec2 const &orig) :spriteSheet(spriteSheet),
                                                                       orig(orig),
																		size(spriteSheet->getSize()){

}

void Scrolling::scroll(float x, float y) {

	orig+= glm::vec2(x, y);
	fixOrig();
}

void Scrolling::fixOrig() {
	fixOneVariable(orig.x);
	fixOneVariable(orig.y);
}

void Scrolling::fixOneVariable(float &a) {
	if(a > 1){
		a = 1 - a;
	}else if( a < 0){
		a = 1 + a;
	}

}

info Scrolling::getInfo() const {
	return info{orig, size};
}

void Scrolling::use() {
	spriteSheet->bindTexture(0);
}
