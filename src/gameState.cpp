//
// Created by stiven on 18-02-27.
//

#include <GL/glew.h>
#include <SDL2/SDL.h>
#include <2dEngine/keyEventHandler.hpp>
#include <2dEngine/eventManager.hpp>
#include <inc/2dEngine/programManager.hpp>
#include <text/displayText.hpp>
#include <entities/Mussel.h>
#include "gameState.hpp"
#include "scene.hpp"
namespace JamEngine {
	GameState GameState::gameState;

	void GameState::loop(SDL_Window *window) {
		while(!gameState.endGame) {

			int start = SDL_GetTicks();
			int end;
			glClear(GL_COLOR_BUFFER_BIT);
			EventManager::execute(gameState.delta);
			gameState.input();
			gameState.update();
			gameState.display();

			SDL_GL_SwapWindow(window);
			end = SDL_GetTicks();
			int sub = end - start;
			if (sub < 17) {
				SDL_Delay(17 - sub);
			}
		}
	}

	void GameState::input() {
		SDL_Event ev;
		while(SDL_PollEvent(&ev)){
			switch(ev.type){
				case SDL_KEYDOWN:
					if(ev.key.keysym.sym == SDLK_ESCAPE){
						gameOver();
					}
					KeyEventHandler::executePressedFunction(ev.key.keysym.sym);
					break;
				case SDL_KEYUP:
					KeyEventHandler::executeReleasedFunction(ev.key.keysym.sym);
					break;
				case SDL_QUIT:
					gameOver();
					break;

				default:break;
			}
		}

	}

	void GameState::update() {
		scene.update();

	}

	void GameState::display() {
		Mussel * m = nullptr;
		ProgramManager::displayBackground();
		for(auto & ent : scene.getEntity()){
			ent->display(delta);
			if(!m){
				m = dynamic_cast<Mussel *>(ent);
			}
			if(isDebug){
				ent->displayDebug(delta);
			}
		}
		if(m) {
			displayText.display(std::to_string(m->getFishEggNumber()), glm::vec2{25, 25}, glm::vec2{50, 50});
		}
	}

	void GameState::gameOver() {
		gameState.endGame = true;
	}

	void GameState::debug() {
		gameState.isDebug = !gameState.isDebug;
	}
}
