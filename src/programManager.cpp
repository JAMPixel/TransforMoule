//
// Created by stiven on 18-03-03.
//

#include <inc/2dEngine/windowSettings.hpp>
#include <inc/2dEngine/spriteSheetManager.hpp>
#include <inc/2dEngine/shapeManager.hpp>
#include "programManager.hpp"
namespace JamEngine{
	void moveCam(float x, float y){
		ProgramManager::moveCam(x, y);
	}
	ProgramManager ProgramManager::programManager;


	glm::mat3 transform(transformStorage const &store, float offsetX,
	                    float offsetY) { //use evocative name to help you navigate in your code
		float cos0 = std::cos(store.angle * (float) M_PI / 180);
		float sin0 = std::sin(store.angle * (float) M_PI / 180);


		glm::mat3 truc{store.scale.x * cos0, -store.scale.x * sin0, 0.0f,
		               store.scale.y * sin0, store.scale.y * cos0, 0.0f,
		               -offsetX * (store.scale.x * cos0 + store.scale.y * sin0) + store.tran.x,
		               -offsetY * (-store.scale.x * sin0 +
		                           store.scale.y * cos0) +
		               store.tran.y, 1.0f
		};
		return truc;
	}



	void ProgramManager::use(const glm::vec2 &orig, const glm::vec2 &textureSize, glm::vec2 const &size, glm::vec2 const &pos,
	float angle) {
		programManager.program.use();
		programManager.program.set<uni::texture>(0);
		programManager.program.set<uni::orig>(orig);
		programManager.program.set<uni::textureSize>(textureSize);
		transformStorage store{(pos-programManager.cam)*programManager.abstractSizeScreen, size*programManager.abstractSizeScreen, angle};
		programManager.program.set<uni::transform>(transform(store));
	}

	void ProgramManager::init(const windowSettings &settings) {
		programManager.program = Program{glish::shaderFile{GL_VERTEX_SHADER, "../shader/vert.glsl"},
		                                 glish::shaderFile{GL_FRAGMENT_SHADER, "../shader/frag.glsl"}};

		programManager.program.init("texture2D", "scale" ,"orig", "textureSize","transform");

		programManager.program.set<uni::scale>(normalizedScreenSpace(settings.width, settings.height));

		programManager.abstractSizeScreen.x = settings.width/16.0f;
		programManager.abstractSizeScreen.y = settings.height/9.0f;

		programManager.scrolling = Scrolling(SpriteSheetManager::get("background"), glm::vec2{0, 0.75});

	}

	void ProgramManager::moveCam(float x, float y) {

		programManager.cam += glm::vec2{x,y};
		programManager.scrolling.scroll(x/16.0f*SpriteSheetManager::get("background")->getNbImageHoriz(),
		                                y/(9.0f*SpriteSheetManager::get("background")->getNbImageVert()));
	}

	void ProgramManager::displayBackground() {
		programManager.program.use();
		ShapeManager::getShape().bind();
		Scrolling &scroll = programManager.scrolling;
		scroll.use();
		auto info = scroll.getInfo();
		programManager.program.set<uni::texture>(0);
		programManager.program.set<uni::orig>(info.orig);
		programManager.program.set<uni::textureSize>(info.size);
		transformStorage store;
		store.tran = glm::vec2{(glm::vec2{8, 4.5})*programManager.abstractSizeScreen};
		store.scale = glm::vec2{1366, 768};
		programManager.program.set<uni::transform>(transform(store));

		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	}

	glm::vec2 const &ProgramManager::getPosCam() {
		return programManager.cam;
	}

	void ProgramManager::raz() {
		programManager.cam = glm::vec2{};
	}
}